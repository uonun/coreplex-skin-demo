﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CorePlex.Controls
{
    /// <summary>
    /// 皮肤风格参数
    /// </summary>
    [Serializable]
    public class SkinOptions : ICloneable, IDisposable
    {
        /// <summary>
        /// 四边阴影的宽度。默认为 6。
        /// </summary>
        public int ShadowWidth { get { return _shadowWidth; } set { _shadowWidth = value; } }
        private int _shadowWidth = 6;

        /// <summary>
        /// 主背景圆角半径。最小值为 1，默认为 4。
        /// </summary>
        public int CornerRadius { get { return _cornerradius; } set { if (value > 5 || value < 0) { _cornerradius = 4; } else { _cornerradius = value; } } }
        private int _cornerradius = 4;
        /*
        /// <summary>
        /// 界面整体透明度。值范围 0~255。
        /// </summary>
        public byte Opacity { get { return _opacity; } set { _opacity = value; } }
        private byte _opacity = 255;
        */
        /// <summary>
        /// 边框宽度。默认为 1。
        /// </summary>
        public int BorderWidth { get { return _borderWidth; } set { _borderWidth = value; } }
        private int _borderWidth = 1;

        /// <summary>
        /// 颜色设定
        /// </summary>
        public ColorShip ColorShip { get { return _colorShip; } set { _colorShip = value; } }
        private ColorShip _colorShip = new ColorShip();

        /// <summary>
        /// 高光过渡照射的角度。默认为“左下角到右上角对角线”的法线方向。
        /// </summary>
        public float BorderHighlightAngle { get { return _borderHighlightAngle; } set { _borderHighlightAngle = value; } }
        private float _borderHighlightAngle = 45f;

        /// <summary>
        /// 窗体背景图
        /// </summary>
        public string BackgroundImagePath { get; set; }
        public Image BackgroundImage
        {
            get
            {
                if (_needReloadBgImg)
                {
                    _bgImg.Dispose();
                    _bgImg = null;
                }

                GetImageBuffer(ref _bgImg, BackgroundImagePath);

                if (_bgImg == null)
                {
                    Bitmap defaultBmp = new Bitmap(200, 100);
                    using (Graphics g = Graphics.FromImage(defaultBmp))
                    {
                        g.FillRectangle(SystemBrushes.Control, 0, 0, 200, 100);
                        g.DrawString("未设置背景图", new Font(new FontFamily("宋体"), 9), SystemBrushes.GrayText, 50, 45);
                        _bgImg = defaultBmp;
                    }

                    this.BackgroundLayout = ImageLayout.Tile;
                    _needReloadBgImg = true;
                }
                return _bgImg;
            }
        }
        private Image _bgImg;
        private bool _needReloadBgImg;

        /// <summary>
        /// 窗体背景图的显示方式
        /// </summary>
        public ImageLayout BackgroundLayout { get { return _backgroundLayout; } set { _backgroundLayout = value; } }
        private ImageLayout _backgroundLayout = ImageLayout.None;

        public string ClientVersion { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Preview { get; set; }
        public string UserPanelBackgroundImagePath { get; set; }
        public Image UserPanelBackgroundImage
        {
            get
            {
                return GetImageBuffer(ref _userPanelBackgroundImage, UserPanelBackgroundImagePath);
            }
        }
        private Image _userPanelBackgroundImage;

        public ImageLayout UserPanelBackgroundImageLayout { get; set; }
        public Size MaximumSize { get; set; }
        public byte Opacity_BgForm { get; set; }
        public byte Opacity_FrontForm { get; set; }

        /// <summary>
        /// 左侧块背景图
        /// </summary>
        public string SidePanelBackgroundImagePath { get; set; }
        public Image SidePanelBackgroundImage
        {
            get
            {
                return GetImageBuffer(ref _sidePanelBackgroundImage, SidePanelBackgroundImagePath);
            }
        }
        private Image _sidePanelBackgroundImage;

        public ImageLayout SidePanelBackgroundImageLayout { get; set; }

        /// <summary>
        /// 左侧主按钮：代码库
        /// </summary>
        public string NavBuLibImagePath { get; set; }
        public Image NavBuLibImage
        {
            get
            {
                return GetImageBuffer(ref _buLibImage, NavBuLibImagePath);
            }
        }
        private Image _buLibImage;
        /// <summary>
        /// 左侧主按钮：搜代码
        /// </summary>
        public string NavBuSearchImagePath { get; set; }
        public Image NavBuSearchImage
        {
            get
            {
                return GetImageBuffer(ref _buSearchImage, NavBuSearchImagePath);
            }
        }
        private Image _buSearchImage;
        /// <summary>
        /// 左侧主按钮鼠标悬停图
        /// </summary>
        public string NavBuHoverImagePath { get; set; }
        public Image NavBuHoverImage
        {
            get
            {
                return GetImageBuffer(ref _buHoverImage, NavBuHoverImagePath);
            }
        }
        private Image _buHoverImage;
        /// <summary>
        /// 左侧导航背景图
        /// </summary>
        public string NavItemBackgroundImagePath { get; set; }
        public Image NavItemBuBackgroundImage
        {
            get
            {
                return GetImageBuffer(ref _navItemBuBackgroundImage, NavItemBackgroundImagePath);
            }
        }
        private Image _navItemBuBackgroundImage;
        /// <summary>
        /// 左侧导航鼠标悬停图
        /// </summary>
        public string NavItemHoverBackgroundImagePath { get; set; }
        public Image NavItemHoverBackgroundImage
        {
            get
            {
                return GetImageBuffer(ref _navItemHoverBackgroundImage, NavItemHoverBackgroundImagePath);
            }
        }
        private Image _navItemHoverBackgroundImage;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Mine { get; set; }
        public Image NavItemImage_Mine
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Mine, NavItemImagePath_Mine);
            }
        }
        private Image _navItemImage_Mine;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Post { get; set; }
        public Image NavItemImage_Post
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Post, NavItemImagePath_Post);
            }
        }
        private Image _navItemImage_Post;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Skin { get; set; }
        public Image NavItemImage_Skin
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Skin, NavItemImagePath_Skin);
            }
        }
        private Image _navItemImage_Skin;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Config { get; set; }
        public Image NavItemImage_Config
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Config, NavItemImagePath_Config);
            }
        }
        private Image _navItemImage_Config;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Favorite { get; set; }
        public Image NavItemImage_Favorite
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Favorite, NavItemImagePath_Favorite);
            }
        }
        private Image _navItemImage_Favorite;
        /// <summary>
        /// 左侧导航项
        /// </summary>
        public string NavItemImagePath_Export { get; set; }
        public Image NavItemImage_Export
        {
            get
            {
                return GetImageBuffer(ref _navItemImage_Export, NavItemImagePath_Export);
            }
        }
        private Image _navItemImage_Export;

        /// <summary>
        /// 构造函数，初始化所有属性的值
        /// </summary>
        public SkinOptions()
        {
            this.ShadowWidth = 6;
            this.CornerRadius = 4;
            //this.Opacity = 255;
            this.BorderWidth = 1;
            this.ColorShip = new ColorShip();
            this.BorderHighlightAngle = 45f;
            this.BackgroundImagePath = string.Empty;
            this.BackgroundLayout = ImageLayout.None;
            this.ClientVersion = "1.0";
            this.Name = string.Empty;
            this.Author = string.Empty;
            this.Preview = "Preview.jpg";
            this.UserPanelBackgroundImagePath = string.Empty;
            this.UserPanelBackgroundImageLayout = ImageLayout.None;
            this.Opacity_BgForm = 255;
            this.Opacity_FrontForm = 255;
            this.SidePanelBackgroundImagePath = string.Empty;
            this.SidePanelBackgroundImageLayout = ImageLayout.None;
        }

        /// <summary>
        /// 将实际图片文件载入为内存图片，不独占图片文件
        /// </summary>
        /// <param name="imgHolder"></param>
        /// <param name="imgFilePath"></param>
        /// <returns></returns>
        public static Image GetImageBuffer(ref Image imgHolder, string imgFilePath)
        {
            if (imgHolder == null)
            {
                if (!string.IsNullOrEmpty(imgFilePath) && File.Exists(imgFilePath))
                {
                    using (Bitmap tmp = new Bitmap(imgFilePath))
                    {
                        imgHolder = (Bitmap)tmp.Clone();
                        tmp.Dispose();
                    }
                }
            }
            return imgHolder;
        }

        #region ICloneable 成员

        public object Clone()
        {
            SkinOptions result = null;
            /*
            using (MemoryStream streamw = new MemoryStream())
            {
                BinaryFormatter f = new BinaryFormatter();
                // 序列化到字节流
                f.Serialize(streamw, this);

                // 重新设置字节流当前位置
                streamw.Position = 0;
                // 反序列化
                result = (SkinOptions)f.Deserialize(streamw);
            }
             * */
            result = new SkinOptions()
            {
                Author = this.Author,
                //BackgroundImage = (Image)this.BackgroundImage.Clone(),
                BackgroundImagePath = this.BackgroundImagePath,
                BackgroundLayout = this.BackgroundLayout,
                BorderHighlightAngle = this.BorderHighlightAngle,
                BorderWidth = this.BorderWidth,
                ClientVersion = this.ClientVersion,
                ColorShip = (ColorShip)this.ColorShip.Clone(),
                CornerRadius = this.CornerRadius,
                MaximumSize = this.MaximumSize,
                Name = this.Name,
                //NavBuHoverImage = this.NavBuHoverImage,
                NavBuHoverImagePath = this.NavBuHoverImagePath,
                //NavBuLibImage = this.NavBuLibImage,
                NavBuLibImagePath = this.NavBuLibImagePath,
                //NavBuSearchImage = this.NavBuSearchImage,
                NavBuSearchImagePath = this.NavBuSearchImagePath,
                NavItemBackgroundImagePath = this.NavItemBackgroundImagePath,
                //NavItemBuBackgroundImage = this.NavItemBuBackgroundImage,
                //NavItemHoverBackgroundImage = this.NavItemHoverBackgroundImage,
                NavItemHoverBackgroundImagePath = this.NavItemHoverBackgroundImagePath,
                //NavItemImage_Config = this.NavItemImage_Config,
                //NavItemImage_Export = this.NavItemImage_Export,
                //NavItemImage_Favorite = this.NavItemImage_Favorite,
                //NavItemImage_Mine = this.NavItemImage_Mine,
                //NavItemImage_Post = this.NavItemImage_Post,
                //NavItemImage_Skin = this.NavItemImage_Skin,
                NavItemImagePath_Config = this.NavItemImagePath_Config,
                NavItemImagePath_Export = this.NavItemImagePath_Export,
                NavItemImagePath_Favorite = this.NavItemImagePath_Favorite,
                NavItemImagePath_Mine = this.NavItemImagePath_Mine,
                NavItemImagePath_Post = this.NavItemImagePath_Post,
                NavItemImagePath_Skin = this.NavItemImagePath_Skin,
                Opacity_BgForm = this.Opacity_BgForm,
                Opacity_FrontForm = this.Opacity_FrontForm,
                Preview = this.Preview,
                ShadowWidth = this.ShadowWidth,
                //SidePanelBackgroundImage = this.SidePanelBackgroundImage,
                SidePanelBackgroundImageLayout = this.SidePanelBackgroundImageLayout,
                SidePanelBackgroundImagePath = this.SidePanelBackgroundImagePath,
                //UserPanelBackgroundImage = this.UserPanelBackgroundImage,
                UserPanelBackgroundImageLayout = this.UserPanelBackgroundImageLayout,
                UserPanelBackgroundImagePath = this.UserPanelBackgroundImagePath
            };

            return result;
        }

        #endregion

        #region IDisposable 成员
        /// <summary>
        /// 释放皮肤相关的图片资源
        /// </summary>
        public void Dispose()
        {
            DisposeImage(ref this._bgImg);
            DisposeImage(ref this._buHoverImage);
            DisposeImage(ref this._buLibImage);
            DisposeImage(ref this._buSearchImage);
            DisposeImage(ref this._navItemBuBackgroundImage);
            DisposeImage(ref this._navItemHoverBackgroundImage);
            DisposeImage(ref this._sidePanelBackgroundImage);
            DisposeImage(ref this._userPanelBackgroundImage);
            /*
            this._buHoverImage.Dispose();
            this._buLibImage.Dispose();
            this._buSearchImage.Dispose();
            this._navBackgroundImage.Dispose();
            this._navHoverBackgroundImage.Dispose();
            this._sidePanelBackgroundImage.Dispose();
            this._userPanelBackgroundImage.Dispose();
             * */
        }
        public static void DisposeImage(ref Image img)
        {
            if (img != null)
            {
                img.Dispose();
                img = null;
            }
        }
        #endregion
    }
}
