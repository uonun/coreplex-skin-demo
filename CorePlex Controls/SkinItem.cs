﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CorePlex.Controls
{
    public partial class SkinItem : UserControl
    {
        [DefaultValue(null)]
        public string Image
        {
            get { return this.pictureBox1.ImageLocation; }
            set
            {
                this.pictureBox1.ImageLocation = value;
                if (File.Exists(value))
                    this.pictureBox1.Load();
            }
        }

        [DefaultValue("SkinName")]
        public string SkinName
        {
            get { return this.lbSkinName.Text; }
            set { this.lbSkinName.Text = value; }
        }

        public new event EventHandler Click
        {
            add
            {
                this.pictureBox1.Click += value;
            }
            remove
            {
                this.pictureBox1.Click -= value;
            }
        }

        public SkinOptions SkinOptions
        {
            get { return (SkinOptions)this.pictureBox1.Tag; }
            set { this.pictureBox1.Tag = value; }
        }

        public string ToolTip
        {
            get { return this.toolTip1.GetToolTip(this); }
            set
            {
                this.toolTip1.SetToolTip(this, value);
                this.toolTip1.SetToolTip(this.pictureBox1, value);
            }
        }

        public new Color BackColor { get; set; }

        public Color HoverBackColor { get; set; }
        public Color HoverForeColor { get; set; }

        public bool IsCurrentSkin
        {
            get { return _isCurrent; }
            set
            {
                _isCurrent = value;
                if (_isCurrent)
                {
                    base.BackColor = Color.Red;
                    this.lbSkinName.ForeColor = Color.Yellow;
                }
                else
                {
                    base.BackColor = this.BackColor;
                    this.lbSkinName.ForeColor = this.ForeColor;
                }
            }
        }
        private bool _isCurrent;

        public SkinItem()
        {
            InitializeComponent();

            this.pictureBox1.InitialImage = DefaultResource.skin_preview;
            this.pictureBox1.ErrorImage = DefaultResource.skin_preview;
            this.Cursor = this.pictureBox1.Cursor = Cursors.Hand;

            this.pictureBox1.MouseEnter += (s, e) =>
            {
                if (_isCurrent) return;
                base.BackColor = this.HoverBackColor;
                this.lbSkinName.ForeColor = this.HoverForeColor;
            };
            this.pictureBox1.MouseLeave += (s, e) =>
            {
                if (_isCurrent) return;
                base.BackColor = this.BackColor;
                this.lbSkinName.ForeColor = this.ForeColor;
            };

            base.BackColor = this.BackColor;
            this.lbSkinName.ForeColor = this.ForeColor;
        }

        private void SkinItem_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.pictureBox1.ImageLocation) && this.pictureBox1.Image == null)
            {
                this.pictureBox1.Image = DefaultResource.skin_preview;
            }
        }
    }
}
