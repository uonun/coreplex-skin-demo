﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 * *********************************************************************** 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using uoLib.Native;

namespace CorePlex.Controls
{
    [ToolboxBitmap(typeof(System.Windows.Forms.Form))]
    public class BeautyForm : Form, IBeautyForm
    {
        #region 公共属性
        /// <summary>
        /// 绑定的前景窗体
        /// </summary>
        public Form FrontForm { get; private set; }

        /// <summary>
        /// 皮肤风格参数
        /// </summary>
        public SkinOptions SkinOptions { get; set; }

        /// <summary>
        /// 获取或设置是否允许通过拖拽窗体的主体部分来移动窗体
        /// </summary>
        public bool DragEnabled { get; set; }

        /// <summary>
        /// 获取或设置是否显示背景遮罩层
        /// </summary>
        public bool IsMarskEnabled { get; set; }

        /// <summary>
        /// 获取或设置要显示的Logo图片
        /// </summary>
        public Bitmap Logo { get; set; }
        #endregion

        #region 常量定义
        /// <summary>
        /// 最小的圆角半径，大于此值才会绘制圆角，否则会按三角形绘制近似角
        /// </summary>
        private const int MinCornerRadius = 3;
        /// <summary>
        /// 控制按钮的高度
        /// </summary>
        private const int ControlBoxHeight = 24;
        /// <summary>
        /// 默认的透明色
        /// </summary>
        public static readonly Color DefaultTransparencyKey = Color.FromArgb(255, 2, 2, 2);
        #endregion

        #region 私有变量
        /// <summary>
        /// 当前高度位置
        /// </summary>
        private int _currentControlBoxImgY = 0;
        /// <summary>
        /// 线程同步上下文
        /// </summary>
        private SynchronizationContext _sc;

        /// <summary>
        /// 原始的背景图片
        /// </summary>
        private Bitmap originalImage = null;
        /// <summary>
        /// 当前显示的背景图（经过阴影、圆角、高光等处理后实际显示的背景图）
        /// </summary>
        private Bitmap bufferdBackgroundImage = null;
        #endregion

        #region 构造、析构

        /// <summary>
        /// 创建一个美化的窗体背景
        /// </summary>
        /// <param name="frontForm">要美化的窗体</param>
        /// <param name="transparencyKey">要显示为透明的颜色，用于透明化前端窗体。不支持透明色。
        /// 为 null 时将默认为 Color.FromArgb(255, 1, 1, 1)。
        /// 注：某些颜色将导致参数<paramref name="dragEnabled"/>失效且表现为 false。</param>
        /// <param name="dragEnabled">是否允许通过拖拽窗体的主体部分来移动窗体</param>
        public BeautyForm(Form frontForm, Color? transparencyKey, bool dragEnabled = true)
        {
            this._sc = SynchronizationContext.Current;

            this.SetStyle(ControlStyles.UserPaint
                        | ControlStyles.AllPaintingInWmPaint
                        | ControlStyles.DoubleBuffer
                        | ControlStyles.ResizeRedraw
                        | ControlStyles.SupportsTransparentBackColor, true);

            this.DragEnabled = dragEnabled;
            this.IsMarskEnabled = false;
            this.FrontForm = frontForm;
            this.FrontForm.BackColor = transparencyKey ?? DefaultTransparencyKey;
            this.FrontForm.TransparencyKey = this.FrontForm.BackColor;
            this.FrontForm.FormBorderStyle = FormBorderStyle.None;
            this.FrontForm.ShowInTaskbar = false;
            this.FrontForm.Load += new EventHandler((object sender, EventArgs e) =>
            {
                DrawBackground(true);
                this.Show();
            });
            this.FrontForm.FormClosing += new FormClosingEventHandler((object sender, FormClosingEventArgs e) =>
            {
                if (!this.isClosing && !this.Disposing && !this.IsDisposed)
                {
                    this.isClosing = true;
                    this.Close();
                }
            });
            this.AddOwnedForm(FrontForm);

            //--------------------------------------------------------

            this.ShowInTaskbar = true;

            // 初始化皮肤参数
            this.SkinOptions = new SkinOptions();

            // 设置一次最小大小，刷新应有的最新大小值，避免窗口被缩得过小
            this.MinimumSize = new Size(1, 1);

            // 初始化高光角度
            this.SkinOptions.BorderHighlightAngle = (float)(Math.Atan((double)this.Width / (double)this.Height) * (180 / Math.PI));

            this.FormClosed += (s, e) =>
            {
                // 还原系统设置的“最大化和最小化时动态显示窗口”的设置
                SetAnimationInfo(_oldAnimation);
            };

            GetAnimationInfo();
        }
        private bool isClosing = false;

        ~BeautyForm()
        {
            this.originalImage = null;
            this.bufferdBackgroundImage = null;
            this.SkinOptions = null;
            this.Logo = null;
        }

        #endregion

        #region 绘制
        private Bitmap CreateBackground()
        {
#if DEBUG
            // 设计模式，无须创建背景
            if (DesignMode)
            {
                return new Bitmap(1, 1);
            }
#endif

            if (this.SkinOptions == null) return new Bitmap(1, 1);

            // 确定最小尺寸，小于该尺寸将会造成绘图区域异常
            int minLen = (this.SkinOptions.ShadowWidth + this.SkinOptions.CornerRadius) * 2 + 1;
            int w = this.Size.Width, h = this.Size.Height;
            if (w < minLen) w = minLen;
            if (h < minLen) h = minLen;
            this.Size = new Size(w, h);

            bufferdBackgroundImage = new Bitmap(Size.Width, Size.Height);
            using (Graphics g = Graphics.FromImage(bufferdBackgroundImage))
            {
                g.SmoothingMode = SmoothingMode.HighQuality;

                DrawShadow(g);
                DrawMain(g);
            }

            return bufferdBackgroundImage;
        }

        /// <summary>
        /// 绘制背景，包括阴影和背景图
        /// </summary>
        /// <param name="resetBgImage">是否重置背景图的缓冲</param>
        /// <param name="currentControlBoxImgY">当前需要显示的控制按钮，在控制按钮资源图上的 Y 位置</param>
        private void DrawBackground(bool resetBgImage = false, int currentControlBoxImgY = 0)
        {
            if (this.WindowState == FormWindowState.Minimized) return;

            if (resetBgImage)
            {
                this._currentControlBoxImgY = currentControlBoxImgY;
                bufferdBackgroundImage = null;
            }

            if (bufferdBackgroundImage == null)
                bufferdBackgroundImage = CreateBackground();

            SetBitmap(bufferdBackgroundImage, this.SkinOptions.Opacity_BgForm);
        }

        /// <summary>
        /// 绘制主背景图
        /// </summary>
        /// <param name="g"></param>
        private void DrawMain(Graphics g)
        {
            // 要显示的区域图像的大小
            Rectangle destRect = new Rectangle(0, 0, this.Width - this.SkinOptions.ShadowWidth * 2 + 1, this.Height - this.SkinOptions.ShadowWidth * 2 + 1);

            // 建立一个临时的 bitmap，用于存放被圆角化的图像
            using (Bitmap corBg = new Bitmap(destRect.Width, destRect.Height))
            {
                using (Graphics corG = Graphics.FromImage(corBg))
                {
                    corG.SmoothingMode = SmoothingMode.HighQuality;

                    // 创建圆角区域
                    using (GraphicsPath gp = CreateRoundRect(destRect, this.SkinOptions.CornerRadius))
                    {
                        Region re = new System.Drawing.Region(gp);

                        // 设置画布区域为圆角区域
                        corG.IntersectClip(re);

                        Pen p = new Pen(this.SkinOptions.ColorShip.BorderColor);
                        p.Width = this.SkinOptions.BorderWidth;
                        p.Alignment = PenAlignment.Inset;

                        switch (this.SkinOptions.BackgroundLayout)
                        {
                            case ImageLayout.Center:
                                {
                                    // 创建源图上的截取区域
                                    Rectangle srcRect = new Rectangle(new Point(0, 0), this.SkinOptions.BackgroundImage.Size);
                                    // 绘制背景图
                                    corG.DrawImage(this.SkinOptions.BackgroundImage, (this.Width - this.SkinOptions.BackgroundImage.Width) / 2, (this.Height - this.SkinOptions.BackgroundImage.Height) / 2, srcRect, GraphicsUnit.Pixel);
                                }
                                break;

                            case ImageLayout.Stretch:
                                {
                                    // 创建源图上的截取区域
                                    Rectangle srcRect = new Rectangle(new Point(0, 0), this.SkinOptions.BackgroundImage.Size);
                                    // 绘制背景图
                                    corG.DrawImage(this.SkinOptions.BackgroundImage, p.Width, p.Width, srcRect, GraphicsUnit.Pixel);
                                }
                                break;

                            case ImageLayout.Tile:
                                {
                                    // 创建源图上的截取区域
                                    TextureBrush tb = new TextureBrush(this.SkinOptions.BackgroundImage);
                                    corG.FillRectangle(tb, destRect);
                                }
                                break;

                            case ImageLayout.Zoom:
                                {
                                    // 创建源图上的截取区域
                                    Rectangle srcRect = new Rectangle(new Point(0, 0), this.SkinOptions.BackgroundImage.Size);
                                    // 绘制背景图
                                    corG.DrawImage(this.SkinOptions.BackgroundImage, (this.Width - this.SkinOptions.BackgroundImage.Width) / 2, (this.Height - this.SkinOptions.BackgroundImage.Height) / 2, srcRect, GraphicsUnit.Pixel);
                                }
                                break;
                            case ImageLayout.None:
                            default:
                                corG.DrawImage(this.SkinOptions.BackgroundImage, p.Width, p.Width);
                                break;
                        }

                        // 构造外边框
                        Rectangle borderOut = new Rectangle(0, 0, destRect.Width - 1, destRect.Height - 1);
                        // 绘制外边框
                        corG.DrawPath(p, CreateRoundRect(borderOut, this.SkinOptions.CornerRadius));

                        // 构造内边框
                        Rectangle borderIn = new Rectangle(1, 1, borderOut.Width - 2, borderOut.Height - 2);

                        using (LinearGradientBrush b = new LinearGradientBrush(borderIn, this.SkinOptions.ColorShip.BorderHighlightColors[0], this.SkinOptions.ColorShip.BorderHighlightColors[1], this.SkinOptions.BorderHighlightAngle))
                        {
                            // 绘制内边框高光
                            using (Pen lightPen = new Pen(b))
                            {
                                // 绘制内边框
                                corG.DrawPath(lightPen, CreateRoundRect(borderIn, this.SkinOptions.CornerRadius));
                            }
                        }

                        // 将圆角图绘制到主画布
                        g.DrawImage(corBg, this.SkinOptions.ShadowWidth, this.SkinOptions.ShadowWidth);
                        /*
                        // 绘制左侧高光
                        using (GraphicsPath gpMask = new GraphicsPath())
                        {
                            Rectangle mask = new Rectangle(this.SkinOptions.BorderWidth + this.SkinOptions.CornerRadius + 1, 120
                                , this.Width - (this.SkinOptions.BorderWidth + this.SkinOptions.CornerRadius + 1) * 2
                                , this.Height - 250);
                            if (mask.Height > 0 && mask.Width > 0)
                            {
                                gpMask.AddRectangle(mask);
                                using (PathGradientBrush b = new PathGradientBrush(gpMask))
                                {
                                    Color[] colors = new Color[2];
                                    float[] positions = new float[2];
                                    ColorBlend sBlend = new ColorBlend();
                                    colors[0] = this.SkinOptions.ColorShip.BackgroundHighlightColors[0];
                                    colors[1] = this.SkinOptions.ColorShip.BackgroundHighlightColors[1];
                                    positions[0] = 0.0f;
                                    positions[1] = 1.0f;
                                    sBlend.Colors = colors;
                                    sBlend.Positions = positions;

                                    b.InterpolationColors = sBlend;
                                    // 上色中心点
                                    b.CenterPoint = new PointF(0, mask.Y + 50);

                                    g.FillPath(b, gpMask);
                                }
                            }
                        }*/

                        #region 绘制Logo
                        using (Bitmap logo = (null == this.Logo) ? DefaultResource.formlogo : (Bitmap)this.Logo.Clone())
                        {
                            Rectangle paintTo = new Rectangle(25, 20, logo.Width, logo.Height);
                            Rectangle sourceRec = new Rectangle(0, 0, logo.Width, logo.Height);
                            g.DrawImage(logo, paintTo, sourceRec, GraphicsUnit.Pixel);
                        }
                        #endregion

                        #region 背景遮罩层
                        if (this.IsMarskEnabled)
                        {
                            int marginTop = 70;  // 遮罩层与顶部的距离
                            int margin = 30;     // 遮罩层与左右的距离之和
                            Brush borderBrush = new SolidBrush(this.SkinOptions.ColorShip.BorderColor); // 边框
                            Brush bgBrush = new SolidBrush(Color.FromArgb(0xcc, 0xff, 0xff, 0xff));     // 填充

                            Rectangle bgmask = new Rectangle();
                            bgmask.Width = this.Width - margin;
                            bgmask.Height = this.Height - margin - marginTop;
                            bgmask.X = (this.Width - bgmask.Width) / 2;
                            bgmask.Y = (this.Height - bgmask.Height) / 2 + marginTop / 2;

                            g.FillRectangle(bgBrush, bgmask);
                            g.DrawRectangle(new Pen(borderBrush), bgmask);
                        }
                        #endregion

                        #region 绘制控制按钮
                        using (Bitmap cb = DefaultResource.controlboxes)
                        {
                            Rectangle paintTo = new Rectangle(0, 0, 0, 0);
                            Rectangle sourceRec = new Rectangle(0, 0, 0, 0);

                            // -:最小化按钮，+:最大化按钮，x:关闭按钮
                            // - + x
                            if (this.MinimizeBox && this.MaximizeBox)
                            {
                                paintTo = new Rectangle(this.Width - this.SkinOptions.ShadowWidth - cb.Width + 3, this.SkinOptions.ShadowWidth - 1, cb.Width, ControlBoxHeight);
                                sourceRec = new Rectangle(0, this._currentControlBoxImgY, cb.Width, ControlBoxHeight);
                            }

                            // x
                            else if (!this.MinimizeBox && !this.MaximizeBox)
                            {
                                paintTo = new Rectangle(this.Width - this.SkinOptions.ShadowWidth - cb.Width + 3 + 56, this.SkinOptions.ShadowWidth - 1, cb.Width - 56, ControlBoxHeight);
                                sourceRec = new Rectangle(56, this._currentControlBoxImgY, cb.Width - 56, ControlBoxHeight);
                            }


                            // + x
                            else if (!this.MinimizeBox && this.MaximizeBox)
                            {
                                paintTo = new Rectangle(this.Width - this.SkinOptions.ShadowWidth - cb.Width + 3 + 28, this.SkinOptions.ShadowWidth - 1, cb.Width - 28, ControlBoxHeight);
                                sourceRec = new Rectangle(28, this._currentControlBoxImgY, cb.Width - 28, ControlBoxHeight);
                            }

                            // - x
                            else if (this.MinimizeBox && !this.MaximizeBox)
                            {
                                paintTo = new Rectangle(this.Width - this.SkinOptions.ShadowWidth - cb.Width + 56, this.SkinOptions.ShadowWidth - 1, cb.Width - 56, ControlBoxHeight);
                                sourceRec = new Rectangle(56, this._currentControlBoxImgY, cb.Width - 56, ControlBoxHeight);
                                g.DrawImage(cb, paintTo, sourceRec, GraphicsUnit.Pixel);

                                paintTo = new Rectangle(this.Width - this.SkinOptions.ShadowWidth - cb.Width + 3 + 26, this.SkinOptions.ShadowWidth - 1, 28, ControlBoxHeight);
                                sourceRec = new Rectangle(0, this._currentControlBoxImgY, 28, ControlBoxHeight);
                            }

                            if (paintTo.Width > 0 && sourceRec.Width > 0)
                                g.DrawImage(cb, paintTo, sourceRec, GraphicsUnit.Pixel);
                        }
                        #endregion
                    }
                }
            }
        }

        /// <summary>
        /// 绘制四角、四边的阴影
        /// </summary>
        /// <param name="g"></param>
        private void DrawShadow(Graphics g)
        {
            /* 阴影分为9宫格，5为内部背景图部分
             *  1   2   3
             *  4   5   6
             *  7   8   9
             */

            // 四角正方形边长 = 圆角半径 + 阴影宽度
            Size corSize = new Size(this.SkinOptions.ShadowWidth + this.SkinOptions.CornerRadius, this.SkinOptions.ShadowWidth + this.SkinOptions.CornerRadius);

            // 左侧、右侧渐变的尺寸
            Size gradientSize_LR = new Size(this.SkinOptions.ShadowWidth, this.Size.Height - corSize.Height * 2);

            // 顶部、底部渐变的尺寸
            Size gradientSize_TB = new Size(this.Size.Width - corSize.Width * 2, this.SkinOptions.ShadowWidth);

            // 绘制四边
            DrawLines(g, corSize, gradientSize_LR, gradientSize_TB);

            // 绘制四角
            DrawCorners(g, corSize);
        }

        /// <summary>
        /// 绘制四角的阴影
        /// </summary>
        /// <param name="g"></param>
        /// <param name="corSize">圆角区域正方形的大小</param>
        /// <returns></returns>
        private void DrawCorners(Graphics g, Size corSize)
        {
            /*
             * 四个角，每个角都是一个扇面
             * 画图时扇面由外弧、内弧以及两段的连接线构成图形
             * 然后在内弧中间附近向外做渐变
             *
             * 阴影分为9宫格，5为内部背景图部分
             *  1   2   3
             *  4   5   6
             *  7   8   9
             */
            Action<int> DrawCorenerN = (n) =>
            {
                using (GraphicsPath gp = new GraphicsPath())
                {
                    // 扇面外沿、内沿曲线的尺寸
                    Size sizeOutSide = new Size(corSize.Width * 2, corSize.Height * 2);
                    Size sizeInSide = new Size(this.SkinOptions.CornerRadius * 2, this.SkinOptions.CornerRadius * 2);

                    // 扇面外沿、内沿曲线的位置
                    Point locationOutSide, locationInSide;
                    // 当圆角半径小于MinCornerRadius时，内沿不绘制曲线，而以线段绘制近似值。该线段绘制方向是从p1指向p2。
                    Point p1, p2;

                    // 渐变起点位置
                    PointF brushCenter;

                    // 扇面起始角度
                    float startAngle;

                    // 根据四个方位不同，确定扇面的位置、角度及渐变起点位置
                    switch (n)
                    {
                        case 1:
                            locationOutSide = new Point(0, 0);
                            startAngle = 180;
                            brushCenter = new PointF((float)sizeOutSide.Width - sizeInSide.Width * 0.5f, (float)sizeOutSide.Height - sizeInSide.Height * 0.5f);
                            p1 = new Point(corSize.Width, this.SkinOptions.ShadowWidth);
                            p2 = new Point(this.SkinOptions.ShadowWidth, corSize.Height);
                            break;

                        case 3:
                            locationOutSide = new Point(this.Width - sizeOutSide.Width, 0);
                            startAngle = 270;
                            brushCenter = new PointF((float)locationOutSide.X + sizeInSide.Width * 0.5f, (float)sizeOutSide.Height - sizeInSide.Height * 0.5f);
                            p1 = new Point(this.Width - this.SkinOptions.ShadowWidth, corSize.Height);
                            p2 = new Point(this.Width - corSize.Width, this.SkinOptions.ShadowWidth);
                            break;

                        case 7:
                            locationOutSide = new Point(0, this.Height - sizeOutSide.Height);
                            startAngle = 90;
                            brushCenter = new PointF((float)sizeOutSide.Width - sizeInSide.Width * 0.5f, (float)locationOutSide.Y + sizeInSide.Height * 0.5f);
                            p1 = new Point(this.SkinOptions.ShadowWidth, this.Height - corSize.Height);
                            p2 = new Point(corSize.Width, this.Height - this.SkinOptions.ShadowWidth);
                            break;

                        default:
                            locationOutSide = new Point(this.Width - sizeOutSide.Width, this.Height - sizeOutSide.Height);
                            startAngle = 0;
                            brushCenter = new PointF((float)locationOutSide.X + sizeInSide.Width * 0.5f, (float)locationOutSide.Y + sizeInSide.Height * 0.5f);
                            p1 = new Point(this.Width - corSize.Width, this.Height - this.SkinOptions.ShadowWidth);
                            p2 = new Point(this.Width - this.SkinOptions.ShadowWidth, this.Height - corSize.Height);
                            break;
                    }

                    // 扇面外沿曲线
                    Rectangle recOutSide = new Rectangle(locationOutSide, sizeOutSide);

                    // 扇面内沿曲线的位置
                    locationInSide = new Point(locationOutSide.X + (sizeOutSide.Width - sizeInSide.Width) / 2, locationOutSide.Y + (sizeOutSide.Height - sizeInSide.Height) / 2);

                    // 扇面内沿曲线
                    Rectangle recInSide = new Rectangle(locationInSide, sizeInSide);

                    // 将扇面添加到形状，以备绘制
                    gp.AddArc(recOutSide, startAngle, 91);

                    if (this.SkinOptions.CornerRadius > MinCornerRadius)
                        gp.AddArc(recInSide, startAngle + 90, -91);
                    else
                        gp.AddLine(p1, p2);

                    // 使用渐变笔刷
                    using (PathGradientBrush shadowBrush = new PathGradientBrush(gp))
                    {
                        Color[] colors = new Color[2];
                        float[] positions = new float[2];
                        ColorBlend sBlend = new ColorBlend();
                        // 扇面外沿色
                        colors[0] = this.SkinOptions.ColorShip.CornerColors[1];
                        // 扇面内沿色
                        colors[1] = this.SkinOptions.ColorShip.CornerColors[0];
                        positions[0] = 0.0f;
                        positions[1] = 1.0f;
                        sBlend.Colors = colors;
                        sBlend.Positions = positions;

                        shadowBrush.InterpolationColors = sBlend;
                        // 上色中心点
                        shadowBrush.CenterPoint = brushCenter;

                        g.FillPath(shadowBrush, gp);
                    }
                }
            };

            DrawCorenerN(1);
            DrawCorenerN(3);
            DrawCorenerN(7);
            DrawCorenerN(9);
        }

        /// <summary>
        /// 绘制上下左右四边的阴影
        /// </summary>
        /// <param name="g"></param>
        /// <param name="corSize"></param>
        /// <param name="gradientSize_LR"></param>
        /// <param name="gradientSize_TB"></param>
        private void DrawLines(Graphics g, Size corSize, Size gradientSize_LR, Size gradientSize_TB)
        {
            Rectangle rect2 = new Rectangle(new Point(corSize.Width, 0), gradientSize_TB);
            Rectangle rect4 = new Rectangle(new Point(0, corSize.Width), gradientSize_LR);
            Rectangle rect6 = new Rectangle(new Point(this.Size.Width - this.SkinOptions.ShadowWidth, corSize.Width), gradientSize_LR);
            Rectangle rect8 = new Rectangle(new Point(corSize.Width, this.Size.Height - this.SkinOptions.ShadowWidth), gradientSize_TB);

            using (
                LinearGradientBrush brush2 = new LinearGradientBrush(rect2, this.SkinOptions.ColorShip.ShadowColors[1], this.SkinOptions.ColorShip.ShadowColors[0], LinearGradientMode.Vertical),
                 brush4 = new LinearGradientBrush(rect4, this.SkinOptions.ColorShip.ShadowColors[1], this.SkinOptions.ColorShip.ShadowColors[0], LinearGradientMode.Horizontal),
                 brush6 = new LinearGradientBrush(rect6, this.SkinOptions.ColorShip.ShadowColors[0], this.SkinOptions.ColorShip.ShadowColors[1], LinearGradientMode.Horizontal),
                 brush8 = new LinearGradientBrush(rect8, this.SkinOptions.ColorShip.ShadowColors[0], this.SkinOptions.ColorShip.ShadowColors[1], LinearGradientMode.Vertical)
            )
            {
                g.FillRectangle(brush2, rect2);
                g.FillRectangle(brush4, rect4);
                g.FillRectangle(brush6, rect6);
                g.FillRectangle(brush8, rect8);
            }
        }

        /// <summary>
        /// 构造圆角路径
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        private GraphicsPath CreateRoundRect(Rectangle rect, int radius)
        {
            GraphicsPath gp = new GraphicsPath();

            int x = rect.X;
            int y = rect.Y;
            int width = rect.Width;
            int height = rect.Height;

            if (width > 0 && height > 0)
            {
                // 半径大才做圆角
                if (radius > MinCornerRadius)
                {
                    radius = Math.Min(radius, height / 2 - 1);
                    radius = Math.Min(radius, width / 2 - 1);
                    gp.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90);
                    gp.AddLine(x + width, y + radius, x + width, y + height - (radius * 2));
                    gp.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
                    gp.AddLine(x + width - (radius * 2), y + height, x + radius, y + height);
                    gp.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
                    gp.AddLine(x, y + height - (radius * 2), x, y + radius);
                    gp.AddArc(x, y, radius * 2, radius * 2, 180, 90);
                }
                else
                {
                    gp.AddLine(x + width - radius, y, x + width, y + radius);
                    gp.AddLine(x + width, y + radius, x + width, y + height - radius);
                    gp.AddLine(x + width, y + height - radius, x + width - radius, y + height);
                    gp.AddLine(x + width - radius, y + height, x + radius, y + height);
                    gp.AddLine(x + radius, y + height, x, y + height - radius);
                    gp.AddLine(x, y + height - radius, x, y + radius);
                    gp.AddLine(x, y + radius, x + radius, y);
                }
                gp.CloseFigure();
            }
            return gp;
        }

        /// <summary>
        /// 绘制已构造好的位图
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="opacity"></param>
        private void SetBitmap(Bitmap bitmap, byte opacity = 255)
        {
            //if (bitmap.PixelFormat != PixelFormat.Format32bppArgb)
            //    throw new Exception("窗体背景图必须是 8 位/通道 RGB 颜色的图。");

            // The ideia of this is very simple,
            // 1. Create a compatible DC with screen;
            // 2. Select the bitmap with 32bpp with alpha-channel in the compatible DC;
            // 3. Call the UpdateLayeredWindow.

            IntPtr screenDc = Win32.GetDC(IntPtr.Zero);
            IntPtr memDc = Win32.CreateCompatibleDC(screenDc);
            IntPtr hBitmap = IntPtr.Zero;
            IntPtr oldBitmap = IntPtr.Zero;

            try
            {
                hBitmap = bitmap.GetHbitmap(Color.FromArgb(0));  // grab a GDI handle from this GDI+ bitmap
                oldBitmap = Win32.SelectObject(memDc, hBitmap);

                Win32.Size size = new Win32.Size(bitmap.Width, bitmap.Height);
                Win32.Point pointSource = new Win32.Point(0, 0);
                Win32.Point topPos = new Win32.Point(Left, Top);
                Win32.BLENDFUNCTION blend = new Win32.BLENDFUNCTION();
                blend.BlendOp = Win32.AC_SRC_OVER;
                blend.BlendFlags = 0;
                blend.SourceConstantAlpha = opacity;
                blend.AlphaFormat = Win32.AC_SRC_ALPHA;

                Win32.UpdateLayeredWindow(Handle, screenDc, ref topPos, ref size, memDc, ref pointSource, 0, ref blend, Win32.ULW_ALPHA);
            }
            finally
            {
                Win32.ReleaseDC(IntPtr.Zero, screenDc);
                if (hBitmap != IntPtr.Zero)
                {
                    Win32.SelectObject(memDc, oldBitmap);
                    Win32.DeleteObject(hBitmap);
                }
                Win32.DeleteDC(memDc);
            }
        }

        /// <summary>
        /// 应用新的皮肤风格（淡入淡出）
        /// </summary>
        /// <param name="newSkin">新皮肤风格。为 null 则取消皮肤</param>
        public void ApplySkin(SkinOptions newSkin)
        {
            if (_isSkinChanging) return;
            this._isSkinChanging = true;
            Cursor oldCursor = this.Cursor;
            this.Cursor = Cursors.WaitCursor;
            // 主背景的淡入渐变因子，数字越大，渐变梯度越大
            byte stepFadein = 5;
            // 主背景的淡出渐变因子，数字越大，渐变梯度越大
            byte stepFadeout = 10;
            // 前景窗体控件的淡入、淡出渐变因子，数字越大，渐变梯度越大
            double stepFrontForm = 0.04;
            // 淡入渐变的线程休眠时间（毫秒），数字越大，渐变越慢
            int speedFadein = 3;
            // 淡出渐变的线程休眠时间（毫秒），数字越大，渐变越慢
            int speedFadeout = 3;

            SkinEventArgs e = new SkinEventArgs() { Old = this.SkinOptions == null ? null : this.SkinOptions, New = newSkin == null ? null : newSkin };

            // 触发皮肤切换之前的事件
            if (OnSkinPreChange != null) { OnSkinPreChange(this, e); }

            if (newSkin == null) { newSkin = new SkinOptions(); }
            // 释放旧皮肤资源
            this.SkinOptions.Dispose();
            // 应用新皮肤
            this.SkinOptions = newSkin;

            SendOrPostCallback check = (a) =>
            {
                _isSkinChanging = false;
                this.Cursor = oldCursor;

                // 触发皮肤切换之后的事件
                if (OnSkinChanged != null) { OnSkinChanged(this, e); }
            };

            // 淡入操作
            SendOrPostCallback show = (a) =>
            {
                byte tmp = 0;
                // 背景的目标透明度
                byte targetOpacity_bg = newSkin.Opacity_BgForm;
                // 前景窗体的目标透明度
                double targetOpacity_form = (double)newSkin.Opacity_FrontForm / 255;
                bufferdBackgroundImage = CreateBackground();
                while (tmp < targetOpacity_bg)
                {
                    SetBitmap(bufferdBackgroundImage, tmp);
                    tmp += stepFadein;

                    if (this.FrontForm.Opacity < targetOpacity_form)
                    {
                        if (this.FrontForm.Opacity + stepFrontForm < targetOpacity_form)
                            this.FrontForm.Opacity += stepFrontForm;
                        else
                            this.FrontForm.Opacity = targetOpacity_form;
                    }

                    Thread.Sleep(speedFadein);
                }

                new Thread((sc) => { ((SynchronizationContext)sc).Post(check, null); }) { Name = "检查切换操作是否完毕" }.Start(this._sc);
            };

            // 淡出操作
            SendOrPostCallback hide = (a) =>
            {
                SkinOptions tmpSkin = (SkinOptions)this.SkinOptions.Clone();
                if (bufferdBackgroundImage != null)
                {
                    Bitmap oldbg = (Bitmap)bufferdBackgroundImage;
                    int currentOpacity = tmpSkin.Opacity_BgForm;
                    if (oldbg != null)
                    {
                        while (currentOpacity > stepFadeout)
                        {
                            if (currentOpacity - (int)stepFadeout <= 0) currentOpacity = stepFadeout;
                            SetBitmap(oldbg, (byte)currentOpacity);
                            currentOpacity -= stepFadeout;

                            if (this.FrontForm.Opacity > 0)
                            {
                                if (this.FrontForm.Opacity - stepFrontForm > 0)
                                    this.FrontForm.Opacity -= stepFrontForm;
                                else
                                {
                                    this.FrontForm.Opacity = 0;
                                }
                            }

                            Thread.Sleep(speedFadeout);
                        }
                    }

                    tmpSkin.Dispose();
                    oldbg.Dispose();
                }

                // 淡出动作完毕时触发此事件
                if (OnSkinFadeout != null) { OnSkinFadeout(this, e); }

                new Thread((sc) => { ((SynchronizationContext)sc).Post(show, this._sc); }) { Name = "淡入新皮肤" }.Start(this._sc);
            };

            new Thread((sc) => { ((SynchronizationContext)sc).Post(hide, this._sc); }) { Name = "淡出旧皮肤" }.Start(this._sc);
        }
        private bool _isSkinChanging = false;
        #endregion

        #region 重写
        #region Show
        public new void Show()
        {
            PreShow();
            base.Show();
        }
        public new void Show(IWin32Window owner)
        {
            PreShow();
            base.Show(owner);
        }
        public new DialogResult ShowDialog()
        {
            PreShow();
            return base.ShowDialog();
        }
        public new DialogResult ShowDialog(IWin32Window owner)
        {
            PreShow();
            return base.ShowDialog(owner);
        }
        private void PreShow()
        {
            // 你可以在这里添加更多在首次显示时需要同步的属性
            this.Size = FrontForm.Size;
            this.Location = FrontForm.Location;
            if (this.WindowState != FormWindowState.Maximized)
                this.WindowState = FrontForm.WindowState;
            this.TopMost = FrontForm.TopMost;
            this.TopLevel = FrontForm.TopLevel;
            this.MinimizeBox = FrontForm.MinimizeBox;
            this.MaximizeBox = FrontForm.MaximizeBox;

            this.Text = FrontForm.Text;
        }
        #endregion

        /// <summary>
        /// 应用当前设置的皮肤背景（立即刷新）
        /// </summary>
        public override void Refresh()
        {
            PreShow();
            DrawBackground(true);
            base.Refresh();
        }

        public override Size MinimumSize
        {
            get
            {
                return base.MinimumSize;
            }
            set
            {
                // 确定最小尺寸，小于该尺寸将会造成绘图区域异常
                // 设置窗体最小大小时，确保窗体不会被缩得过小
                int minLen = (this.SkinOptions.ShadowWidth + this.SkinOptions.CornerRadius) * 2 + 1;
                if (value.Width < minLen) value.Width = minLen;
                if (value.Height < minLen) value.Height = minLen;
                base.MinimumSize = value;
            }
        }

        protected override void OnShown(EventArgs e)
        {
            // 第一次显示时，需要第一次绘制
            if (!this.DesignMode)
            {
                this.StartPosition = FrontForm.StartPosition;
                this.MinimumSize = FrontForm.MinimumSize;
                this.MaximumSize = FrontForm.MaximumSize;

                // 禁用“最大化和最小化时动态显示窗口”
                SetAnimationInfo(0);
                DrawBackground(true);
            }

            base.OnShown(e);
        }

        private bool needRebuildBgImg = false;
        protected override void OnResizeBegin(EventArgs e)
        {
            //Debug.WriteLine("OnResizeBegin: " + this.Size);
            needRebuildBgImg = true;
            base.OnResizeBegin(e);
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            //Debug.WriteLine("OnResizeEnd: " + this.Size);
            needRebuildBgImg = false;
            base.OnResizeEnd(e);
        }

        /// <summary>
        /// 同步大小的更改，并重绘背景窗体
        /// </summary>
        /// <param name="e"></param>
        protected override void OnResize(EventArgs e)
        {
            // 更改大小时，需要重新绘制
            if (!this.DesignMode)
            {
                if (this.Visible)
                {
                    this.SuspendLayout();
                    this.FrontForm.SuspendLayout();

                    needRebuildBgImg = needRebuildBgImg || this.WindowState != this.FrontForm.WindowState;

                    if (this.WindowState != FormWindowState.Minimized)
                    {
                        // 禁用“最大化和最小化时动态显示窗口”
                        SetAnimationInfo(0);
                    }

                    if (!this.FrontForm.Bounds.Equals(this.Bounds)) this.FrontForm.Bounds = this.Bounds;
                    if (!this.FrontForm.Size.Equals(this.Size)) this.FrontForm.Size = this.Size;

                    //最大化时特殊处理，不对前景窗体进行最大化
                    if (this.WindowState == FormWindowState.Maximized)
                    {
                        Win32.ShowWindow(new HandleRef(this, this.Handle), 3);
                        //FrontForm.Location = new Point(-8, -8);
                    }
                    else// if (this.WindowState == FormWindowState.Minimized)
                    {
                        if (this.FrontForm.WindowState != this.WindowState)
                            this.FrontForm.WindowState = this.WindowState;
                    }

                    // 定义背景窗体应当显示的区域（For WinServer2003）
                    // this.Region = new Region(f.ClientRectangle);

                    DrawBackground(needRebuildBgImg);

                    this.FrontForm.ResumeLayout();
                    this.ResumeLayout();
                }
            }
            base.OnResize(e);
        }

        /// <summary>
        /// 移动时的位置同步
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLocationChanged(EventArgs e)
        {
            this.FrontForm.Location = this.Location;
            base.OnLocationChanged(e);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                // 绘制背景必须针对具有 WS_EX_LAYERED 扩展风格的窗体进行
                if (!this.DesignMode)
                {
                    cp.ExStyle |= Win32.WS_EX_LAYERED; // WS_EX_LAYERED
                }
                return cp;
            }
        }

        protected override void WndProc(ref Message m)
        {
            /* 
             * 鼠标移动、按下、释放的消息
             * 对鼠标所在区域的消息进行处理，实现在窗体内部按住鼠标拖动窗体的效果。
             * 同时，此处也对鼠标在边框附近的情况做了判断，用以正常调整窗体大小
             * 注意：由于最小化、最大化、关闭按钮的区域已被处理识别为标题栏区域，
             * 因此这三个按钮将不存在。否则请自行识别这三个按钮的位置区域。
            */
            if (m.Msg == (int)Win32.WM_Messages.WM_NCHITTEST)
            {
                #region 鼠标移动、按下、释放的消息

                // WM_NCHITTEST 消息，LParam 参数的低16位为鼠标的X坐标，高16位为鼠标的Y坐标（相对于屏幕左上角）
                int lParam = m.LParam.ToInt32();
                int x = (lParam << 16 >> 16) - this.Location.X;
                int y = (lParam >> 16) - this.Location.Y;

                // 检查边框区域
                bool checkBorder = true;

                // 全屏状态不检查边框区域
                if (this.WindowState != FormWindowState.Maximized)
                {
                    #region 处理拖动和调整大小
                    // 设置边框检查区域的宽度，即边框宽度。
                    int border = this.SkinOptions.ShadowWidth + 2;

                    // 鼠标在边框区域内，检测为ReSize操作；
                    // 否则检测为标题区域（目的是拖动窗体）
                    if (x < border)
                    {
                        if (y < border)
                        {
                            // ↖
                            m.Result = new IntPtr(Win32.HTTOPLEFT);
                        }
                        else if (y > this.Height - border)
                        {
                            // ↙
                            m.Result = new IntPtr(Win32.HTBOTTOMLEFT);
                        }
                        else
                        {
                            // ←
                            m.Result = new IntPtr(Win32.HTLEFT);
                        }
                    }
                    else if (x > this.Width - border)
                    {
                        if (y < border)
                        {
                            // ↗
                            m.Result = new IntPtr(Win32.HTTOPRIGHT);
                        }
                        else if (y > this.Height - border)
                        {
                            // ↘
                            m.Result = new IntPtr(Win32.HTBOTTOMRIGHT);
                        }
                        else
                        {
                            // →
                            m.Result = new IntPtr(Win32.HTRIGHT);
                        }
                    }
                    else
                    {
                        if (y < border)
                        {
                            // ↑
                            m.Result = new IntPtr(Win32.HTTOP);
                        }
                        else if (y > this.Height - border)
                        {
                            // ↓
                            m.Result = new IntPtr(Win32.HTBOTTOM);
                        }
                        else
                        {
                            checkBorder = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    checkBorder = false;
                }

                // 非边框区域，则检查是否是在控制按钮区域
                if (!checkBorder)
                {
                    int cbImgY = 0;
                    if (x > this.Width - this.SkinOptions.ShadowWidth - 102 && y < this.SkinOptions.ShadowWidth + 20)
                    {
                        if (x - (this.Width - this.SkinOptions.ShadowWidth - 102) < 32)
                        {
                            if (this.MinimizeBox)
                            {
                                cbImgY = 1 * ControlBoxHeight;
                                m.Result = (IntPtr)Win32.HTMINBUTTON;
                            }
                        }
                        else if (x > this.Width - this.SkinOptions.ShadowWidth - 41)
                        {
                            cbImgY = 3 * ControlBoxHeight;
                            m.Result = (IntPtr)Win32.HTCLOSE;
                        }
                        else
                        {
                            if (this.MaximizeBox)
                            {
                                cbImgY = 2 * ControlBoxHeight;
                                m.Result = (IntPtr)Win32.HTMAXBUTTON;
                            }
                        }
                    }
                    else
                    {
                        if (DragEnabled)
                        {
                            //// 检测为标题区域（目的是拖动窗体）
                            m.Result = (IntPtr)Win32.HTCAPTION;
                        }
                        // 检测为Body区域
                        //m.Result = (IntPtr)Win32.HTCLIENT;

                    }

                    // 只有鼠标在控制按钮区域时才重绘背景
                    if (this._currentControlBoxImgY != cbImgY)
                        DrawBackground(true, cbImgY);
                }
                #endregion

                return;
            }

            // 鼠标移出窗体
            else if (m.Msg == (int)Win32.WM_Messages.WM_NCMOUSELEAVE)
            {
                // 只有从控制按钮处移出窗体才需要重绘背景
                if (this._currentControlBoxImgY != 0)
                    DrawBackground(true);
            }

            else if (m.Msg == (int)Win32.WM_Messages.WM_SYSCOMMAND)
            {
                ////由于处理鼠标所在位置的消息时，将窗体内部区域处理为标题区域，
                ////因此此处拦截对窗体内部区域双击时的界面最大化
                //long wparam = m.WParam.ToInt64();
                //if ((wparam & Win32.SC_RESTORE) == Win32.SC_RESTORE)
                //{
                //    Debug.WriteLine(m);
                //    return;
                //}
            }

            base.WndProc(ref m);
        }
        #endregion

        #region 取消“最大化和最小化时动态显示窗口”
        /* 避免“最大化和最小化时动态显示窗口”，此效果会导致还原窗体时，背景窗体滞后于前景窗体显示
         * 要取消这个动态显示窗口的效果，可以有两个方法：
         * 
         * 方法一：重写 CreateParams 属性
         *      // 将窗体的样式设为 POPUP，而非 CAPTION。
         *      // 0x960f << 16（即0x960f0000） 这个值只是从迅雷窗体上抓出来的。。
         *      cp.Style = 0x960f << 16;
         *      但此方法会导致 WndProc 消息循环中，最小化、最大化两个按钮失效。
         * 
         * 方法二：通过API，禁用动态效果
         *      （即此次的处理方式）
         */
        private int _oldAnimation = -1;
        private int GetAnimationInfo()
        {
            uint size = (uint)Marshal.SizeOf(typeof(Win32.ANIMATIONINFO));
            Win32.ANIMATIONINFO ani = new Win32.ANIMATIONINFO();
            ani.cbSize = size;
            bool x = Win32.SystemParametersInfo(Win32.SPI_GETANIMATION, ani.cbSize, ref ani, 0U);
            if (_oldAnimation == -1) _oldAnimation = ani.iMinAnimate;

            return ani.iMinAnimate;
        }

        /// <summary>
        /// 设置最小化和最大化时的动态显示窗口。
        /// </summary>
        /// <param name="ani"></param>
        private void SetAnimationInfo(int animation)
        {
            int _old = GetAnimationInfo();
            if (_old == animation) return;

            Win32.ANIMATIONINFO ani = new Win32.ANIMATIONINFO(animation);
            bool x = Win32.SystemParametersInfo(Win32.SPI_SETANIMATION, ani.cbSize, ref ani, Win32.SPIF_UPDATEINIFILE | Win32.SPIF_SENDCHANGE);
        }
        #endregion

        /// <summary>
        /// 应用新皮肤风格前触发的事件
        /// </summary>
        public event SkinHandler OnSkinPreChange;
        /// <summary>
        /// 淡出动作完毕时触发此事件
        /// </summary>
        public event SkinHandler OnSkinFadeout;
        /// <summary>
        /// 新皮肤风格应用完毕后触发的事件
        /// </summary>
        public event SkinHandler OnSkinChanged;
    }

    public delegate void SkinHandler(object sender, SkinEventArgs e);

    /// <summary>
    /// 皮肤更换参数
    /// </summary>
    public class SkinEventArgs : EventArgs
    {
        /// <summary>
        /// 旧皮肤风格的Clone副本
        /// </summary>
        public SkinOptions Old;
        /// <summary>
        /// 新皮肤风格的Clone副本
        /// </summary>
        public SkinOptions New;
    }

}
