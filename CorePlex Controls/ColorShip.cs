﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/gpl.html.
 * 
 ***********************************************************************
 * 推荐使用：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Drawing;

namespace CorePlex.Controls
{
    [Serializable]
    public class ColorShip : ICloneable
    {
        /// <summary>
        /// 底部版权信息文字颜色
        /// </summary>
        public Color CopyrightTextColor = Color.White;
        /// <summary>
        /// 皮肤信息文字颜色
        /// </summary>
        public Color SkinInfoTextColor = Color.White;
        /// <summary>
        /// 边框颜色
        /// </summary>
        public Color BorderColor = Color.FromArgb(66, 97, 178);
        /// <summary>
        /// 四边阴影的颜色。[0]为阴影内沿颜色，[1]为阴影外沿颜色
        /// </summary>
        public Color[] ShadowColors = { Color.FromArgb(60, 0, 0, 0), Color.FromArgb(0, 0, 0, 0) };
        /// <summary>
        /// 圆角阴影的颜色。[0]为阴影内沿颜色，[1]为阴影外沿颜色。
        /// 注：一般来讲，圆角阴影内沿的颜色应当比四边阴影内沿的颜色更深，才会有更好的显示效果。此值应当根据您的实际情况而定。
        /// </summary>
        /// <remarks>由于给扇面上渐变时，起点并不是准确的扇面内弧，因此扇面的内沿颜色可能应比四边的内沿颜色深</remarks>
        public Color[] CornerColors = { Color.FromArgb(180, 0, 0, 0), Color.FromArgb(0, 0, 0, 0) };
        /// <summary>
        /// 高光颜色。[0]为高光边框左上角点的颜色，[1]为高光边框右下角的颜色
        /// </summary>
        public Color[] BorderHighlightColors = { Color.FromArgb(200, 255, 255, 255), Color.FromArgb(200, 255, 255, 255) };
        /// <summary>
        /// 背景高光。[0]为上下两端的颜色，[1]为中间高光的颜色
        /// </summary>
        public Color[] BackgroundHighlightColors = { Color.FromArgb(0, 255, 255, 255), Color.FromArgb(200, 255, 255, 255) };
        /// <summary>
        /// 用户块背景颜色
        /// </summary>
        public Color UserPanelBackgroundColor = Color.White;
        /// <summary>
        /// 用户块-用户名文字颜色
        /// </summary>
        public Color UserPanelUserNameTextColor = Color.Red;
        /// <summary>
        /// 用户块-普通文字颜色
        /// </summary>
        public Color UserPanelUserInfoTextColor = Color.FromArgb(128, 128, 128);
        /// <summary>
        /// 左侧块背景色
        /// </summary>
        public Color SidePanelBackgroundColor = Color.White;
        /// <summary>
        /// 左侧块边框色
        /// </summary>
        public Color SidePanelBorderColor = Color.White;
        /// <summary>
        /// 左侧主按钮文本颜色
        /// </summary>
        public Color NavBuTextColor = Color.Black;
        /// <summary>
        /// 左侧项文本颜色
        /// </summary>
        public Color NavItemTextColor = Color.Black;
        /// <summary>
        /// 左侧项鼠标悬停文本颜色
        /// </summary>
        public Color NavItemTextHoverColor = Color.White;

        #region ICloneable 成员

        public object Clone()
        {
            ColorShip result = null;
            /*using (MemoryStream streamw = new MemoryStream())
            {
                BinaryFormatter f = new BinaryFormatter();
                // 序列化到字节流
                f.Serialize(streamw, this);

                // 重新设置字节流当前位置
                streamw.Position = 0;
                // 反序列化
                result = (ColorShip)f.Deserialize(streamw);
            }*/
            result = new ColorShip()
            {
                BackgroundHighlightColors = (Color[])this.BackgroundHighlightColors.Clone(),
                BorderColor = this.BorderColor,
                BorderHighlightColors = (Color[])this.BorderHighlightColors.Clone(),
                CopyrightTextColor = this.CopyrightTextColor,
                CornerColors = (Color[])this.CornerColors.Clone(),
                NavBuTextColor = this.NavBuTextColor,
                NavItemTextColor = this.NavItemTextColor,
                NavItemTextHoverColor = this.NavItemTextHoverColor,
                ShadowColors = (Color[])this.ShadowColors.Clone(),
                SidePanelBackgroundColor = this.SidePanelBackgroundColor,
                SidePanelBorderColor = this.SidePanelBorderColor,
                SkinInfoTextColor = this.SkinInfoTextColor,
                UserPanelBackgroundColor = this.UserPanelBackgroundColor,
                UserPanelUserInfoTextColor = this.UserPanelUserInfoTextColor,
                UserPanelUserNameTextColor = this.UserPanelUserNameTextColor
            };

            return result;
        }

        #endregion
    }
}
