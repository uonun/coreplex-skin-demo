﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 * ***********************************************************************/ 

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace CorePlex.Common
{
    [Serializable]
    public class Settings
    {
        public string SkinName { get; set; }

        public Settings()
        {
            _xml = GetSettingsXml();
            this.SkinName = GetNodeValue("Skin", "春意盎然");
        }

        /// <summary>
        /// 获取程序启动目录。
        /// </summary>
        public string StartupPath
        {
            get
            {
                return Application.StartupPath;
            }
        }

        #region 获取和保存XML配置
        private static XmlDocument _xml;
        private XmlDocument GetSettingsXml()
        {
            if (_xml == null)
            {
                string filepath = string.Format("{0}\\Profile\\Config.xml", this.StartupPath);

                try
                {
                    _xml = new XmlDocument();
                    _xml.Load(filepath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("获取配置文件出错，已恢复默认设置！");
                    _xml.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" ?><CorePlex></CorePlex>");
                    FileInfo fi = new FileInfo(filepath);
                    if (!fi.Directory.Exists) fi.Directory.Create();
                    _xml.Save(filepath);
                }
            }
            return _xml;
        }

        /// <summary>
        /// 获取配置节的值
        /// </summary>
        /// <param name="xPath"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private string GetNodeValue(string xPath, string defaultValue, string xPathHead = "//CorePlex/")
        {
            XmlNode node = _xml.SelectSingleNode(string.Format("{0}{1}", xPathHead, xPath));
            if (node == null || !node.HasChildNodes) return defaultValue;
            return node.FirstChild.Value;
        }

        public void Save()
        {
            string filepath = string.Format("{0}\\Profile\\Config.xml", this.StartupPath);
            XmlNode root = _xml.SelectSingleNode("//CorePlex");
            SetNodeValue(root, "Skin", this.SkinName);

            _xml.Save(filepath);
        }

        private void SetNodeValue(XmlNode root, string nodeName, string nodeValue)
        {
            XmlNode node = _xml.SelectSingleNode("//CorePlex/" + nodeName);
            if (node == null)
            {
                XmlElement child = _xml.CreateElement(nodeName);
                child.InnerText = nodeValue;
                root.AppendChild(child);
            }
            else
            {
                node.FirstChild.Value = nodeValue;
            }
        }
        #endregion

    }
}