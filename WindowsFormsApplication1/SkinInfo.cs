﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using CorePlex.Common;
using CorePlex.Controls;

namespace CorePlex.Core
{
    [Serializable]
    public class SkinInfo : SkinOptions
    {
        /// <summary>
        /// 持有所有皮肤信息
        /// </summary>
        public static Dictionary<string, SkinInfo> AllSkins;
        private Settings settings;
        private string _skinRootPath;

        /// <summary>
        /// 当前皮肤所在文件夹
        /// </summary>
        public string SkinFolderPath { get; set; }

        public SkinInfo(Settings settings)
        {
            this.settings = settings;
            this._skinRootPath = string.Format("{0}\\skin", settings.StartupPath);
        }

        ~SkinInfo()
        {
            this.ColorShip = null;
        }

        public Dictionary<string, SkinInfo> GetAllSkins(bool reload = false)
        {
            if (reload == false)
                if (AllSkins != null && AllSkins.Count > 0) return AllSkins;

            AllSkins = new Dictionary<string, SkinInfo>();
            if (Directory.Exists(_skinRootPath))
            {
                string[] folders = Directory.GetDirectories(_skinRootPath);
                foreach (string f in folders)
                {
                    SkinInfo s = GetSkin("", folderFullName: f);
                    if (s != null)
                    {
                        // 重名皮肤
                        while (AllSkins.ContainsKey(s.Name))
                        {
                            s.Name += "_2";
                        }
                        AllSkins.Add(s.Name, s);
                    }
                }
            }
            return AllSkins;
        }

        public SkinInfo GetSkin(string skinName, string folderFullName = "")
        {
            SkinInfo s = null;

            if (AllSkins == null) AllSkins = GetAllSkins(true);
            if (AllSkins != null && AllSkins.Count > 0)
            {
                if (AllSkins.ContainsKey(skinName))
                    s = AllSkins[skinName];
            }

            if (s == null)
            {
                try
                {
                    if (string.IsNullOrEmpty(folderFullName)) return null;

                    s = new SkinInfo(this.settings);
                    s.SkinFolderPath = folderFullName;
                    XmlDocument xml = new XmlDocument();
                    xml.Load(string.Format("{0}\\skin.xml", folderFullName));
                    XmlNode node = xml.SelectSingleNode("//coreplex/skin");

                    XmlAttribute attr;
                    XmlNode tmpNode;
                    attr = node.Attributes["client"];
                    if (attr != null)
                    {
                        s.ClientVersion = attr.Value;
                    }

                    s.Name = node.Attributes["name"].Value;

                    attr = node.Attributes["max"];
                    if (attr != null)
                    {
                        string size = attr.Value;
                        if (!string.IsNullOrEmpty(size))
                        {
                            string[] wh = size.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            if (wh.Length == 2)
                            {
                                s.MaximumSize = new Size(int.Parse(wh[0]), int.Parse(wh[1]));
                            }
                        }
                    }

                    attr = node.Attributes["author"];
                    if (attr != null)
                    {
                        s.Author = attr.Value;
                    }

                    tmpNode = node.SelectSingleNode("preview");
                    if (tmpNode != null)
                    {
                        s.Preview = tmpNode.Attributes["src"].Value;
                    }

                    tmpNode = node.SelectSingleNode("bg");
                    if (tmpNode != null)
                    {
                        s.BackgroundImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                        attr = tmpNode.Attributes["layout"];
                        if (attr != null)
                        {
                            if (string.IsNullOrEmpty(attr.Value))
                                s.BackgroundLayout = ImageLayout.None;
                            else
                                s.BackgroundLayout = (ImageLayout)Enum.Parse(typeof(ImageLayout), attr.Value);
                        }
                    }

                    tmpNode = node.SelectSingleNode("userpanelbg");
                    if (tmpNode != null)
                    {
                        s.UserPanelBackgroundImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                        attr = tmpNode.Attributes["layout"];
                        if (attr != null)
                        {
                            if (string.IsNullOrEmpty(attr.Value))
                                s.UserPanelBackgroundImageLayout = ImageLayout.None;
                            else
                                s.UserPanelBackgroundImageLayout = (ImageLayout)Enum.Parse(typeof(ImageLayout), attr.Value);
                        }
                    }

                    tmpNode = node.SelectSingleNode("sidebg");
                    if (tmpNode != null)
                    {
                        s.SidePanelBackgroundImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                        attr = tmpNode.Attributes["layout"];
                        if (attr != null)
                        {
                            if (string.IsNullOrEmpty(attr.Value))
                                s.SidePanelBackgroundImageLayout = ImageLayout.None;
                            else
                                s.SidePanelBackgroundImageLayout = (ImageLayout)Enum.Parse(typeof(ImageLayout), attr.Value);
                        }
                    }

                    tmpNode = node.SelectSingleNode("bulib");
                    if (tmpNode != null)
                    {
                        s.NavBuLibImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("busearch");
                    if (tmpNode != null)
                    {
                        s.NavBuSearchImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("buhoverbg");
                    if (tmpNode != null)
                    {
                        s.NavBuHoverImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("navbg");
                    if (tmpNode != null)
                    {
                        s.NavItemBackgroundImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("navhoverbg");
                    if (tmpNode != null)
                    {
                        s.NavItemHoverBackgroundImagePath = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_mine");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Mine = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_post");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Post = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_skin");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Skin = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_config");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Config = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_favorite");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Favorite = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("nav_export");
                    if (tmpNode != null)
                    {
                        s.NavItemImagePath_Export = string.Format("{0}\\{1}", folderFullName, tmpNode.Attributes["src"].Value);
                    }

                    tmpNode = node.SelectSingleNode("opacity0");
                    if (tmpNode != null)
                    {
                        s.Opacity_BgForm = byte.Parse(tmpNode.Attributes["value"].Value);
                    }

                    tmpNode = node.SelectSingleNode("opacity1");
                    if (tmpNode != null)
                    {
                        s.Opacity_FrontForm = byte.Parse(tmpNode.Attributes["value"].Value);
                    }

                    tmpNode = node.SelectSingleNode("cornerradius");
                    if (tmpNode != null)
                    {
                        s.CornerRadius = byte.Parse(tmpNode.Attributes["value"].Value);
                    }

                    tmpNode = node.SelectSingleNode("colorship");
                    #region [- ColorShip -]
                    if (tmpNode != null)
                    {
                        Color? color;

                        color = GetColor(tmpNode, "skininfo");
                        if (color != null) s.ColorShip.SkinInfoTextColor = (Color)color;

                        color = GetColor(tmpNode, "copyright");
                        if (color != null) s.ColorShip.CopyrightTextColor = (Color)color;

                        color = GetColor(tmpNode, "border");
                        if (color != null) s.ColorShip.BorderColor = (Color)color;

                        color = GetColor(tmpNode, "shadow0");
                        if (color != null) s.ColorShip.ShadowColors[0] = (Color)color;

                        color = GetColor(tmpNode, "shadow1");
                        if (color != null) s.ColorShip.ShadowColors[1] = (Color)color;

                        color = GetColor(tmpNode, "corner0");
                        if (color != null) s.ColorShip.CornerColors[0] = (Color)color;

                        color = GetColor(tmpNode, "corner1");
                        if (color != null) s.ColorShip.CornerColors[1] = (Color)color;

                        color = GetColor(tmpNode, "borderhightlight0");
                        if (color != null) s.ColorShip.BorderHighlightColors[0] = (Color)color;

                        color = GetColor(tmpNode, "borderhightlight1");
                        if (color != null) s.ColorShip.BorderHighlightColors[1] = (Color)color;

                        color = GetColor(tmpNode, "backgroundhighlight0");
                        if (color != null) s.ColorShip.BackgroundHighlightColors[0] = (Color)color;

                        color = GetColor(tmpNode, "backgroundhighlight1");
                        if (color != null) s.ColorShip.BackgroundHighlightColors[1] = (Color)color;

                        color = GetColor(tmpNode, "userpanelbg");
                        if (color != null) s.ColorShip.UserPanelBackgroundColor = (Color)color;

                        color = GetColor(tmpNode, "userpanelname");
                        if (color != null) s.ColorShip.UserPanelUserNameTextColor = (Color)color;

                        color = GetColor(tmpNode, "userpanelinfo");
                        if (color != null) s.ColorShip.UserPanelUserInfoTextColor = (Color)color;

                        color = GetColor(tmpNode, "sidebg");
                        if (color != null) s.ColorShip.SidePanelBackgroundColor = (Color)color;

                        color = GetColor(tmpNode, "sideborder");
                        if (color != null) s.ColorShip.SidePanelBorderColor = (Color)color;

                        color = GetColor(tmpNode, "navbutext");
                        if (color != null) s.ColorShip.NavBuTextColor = (Color)color;

                        color = GetColor(tmpNode, "navitemtext");
                        if (color != null) s.ColorShip.NavItemTextColor = (Color)color;

                        color = GetColor(tmpNode, "navitemhovertext");
                        if (color != null) s.ColorShip.NavItemTextHoverColor = (Color)color;
                    }
                    #endregion

                    xml = null;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return s;
        }

        /// <summary>
        /// 根据节点获取颜色值
        /// </summary>
        /// <param name="node"></param>
        /// <param name="nodename"></param>
        /// <returns></returns>
        private Color? GetColor(XmlNode node, string nodename)
        {
            XmlNode tmpNode = node.SelectSingleNode(nodename);
            if (tmpNode != null)
            {
                int a = int.Parse(tmpNode.Attributes["a"].Value);
                int r = int.Parse(tmpNode.Attributes["r"].Value);
                int g = int.Parse(tmpNode.Attributes["g"].Value);
                int b = int.Parse(tmpNode.Attributes["b"].Value);
                return Color.FromArgb(a, r, g, b);
            }
            return null;
        }

        public SkinInfo GetFirst()
        {
            if (AllSkins != null && AllSkins.Count > 0)
            {
                var ie = AllSkins.Values.GetEnumerator();
                if (ie.MoveNext())
                    return ie.Current;
            }
            return new SkinInfo(this.settings);
            //throw new Exception("找不到任何皮肤文件，请下载安装皮肤或重新安装插件");
        }
    }
}
