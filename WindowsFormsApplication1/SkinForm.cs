﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 ***********************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using CorePlex.Controls;
using WindowsFormsApplication1;

namespace CorePlex.Core
{
    public partial class SkinForm : Form, IBeautyForm
    {
        /// <summary>
        /// 背景窗体
        /// </summary>
        private BeautyForm bgForm;

        public SkinForm()
        {
            if (!DesignMode)
            {
                this.bgForm = new BeautyForm(this, null);
                this.bgForm.Logo = WindowsFormsApplication1.Properties.Resources.formlogo_skin;

                // 显示中间的背景遮罩层
                this.bgForm.IsMarskEnabled = true;
                this.bgForm.SkinOptions = MainForm._CurrentSkin;
            }

            InitializeComponent();

            /*
             * 设定Padding，从而使布局区域与背景窗体上的背景遮罩层重叠
             */
            this.Padding = new System.Windows.Forms.Padding(16, 86, 15, 15);

            this.Icon = bgForm.Icon = WindowsFormsApplication1.Properties.Resources.coreplex_ico;
            this.Text = string.Format("CorePlex Skin Demo [{0}]", MainForm._CurrentSkin.Name);

            this.SuspendLayout();
            foreach (var s in SkinInfo.AllSkins)
            {
                AddItem(s, (s1, e1) =>
                {
                    SkinOptions so = ((PictureBox)s1).Tag as SkinOptions;
                    if (so != null)
                    {
                        foreach (var item in ((PictureBox)s1).Parent.Parent.Controls)
                        {
                            SkinItem i = (SkinItem)item;
                            if (i.SkinName == so.Name)
                                i.IsCurrentSkin = true;
                            else
                                i.IsCurrentSkin = false;
                        }

                        foreach (IBeautyForm f in Application.OpenForms)
                        {
                            f.ApplySkin(so);
                        }

                        MainForm._CurrentSkin = (SkinInfo)so;
                        MainForm._Settings.SkinName = so.Name;
                        MainForm._Settings.Save();
                        Trace.TraceInformation("{0:HH:mm:ss ffffff} > 设置皮肤：{1}", DateTime.Now, so.Name);
                        GC.Collect();
                    }
                });
            }
            this.ResumeLayout();
        }

        #region IBeautyForm 成员

        void IBeautyForm.ApplySkin(SkinOptions newSkin)
        {
            this.bgForm.ApplySkin(newSkin);
        }

        SkinOptions IBeautyForm.SkinOptions
        {
            get { return this.bgForm.SkinOptions; }
        }

        #endregion

        private void SkinForm_Load(object sender, EventArgs e)
        {
            AddItem(new KeyValuePair<string, SkinInfo>(
                "更多精彩皮肤下载",
                 new SkinInfo(MainForm._Settings) { Name = "点击访问官方网站皮肤中心" }
            ), (s, e1) => { Process.Start("http://www.udnz.com/addin-skin.htm"); });
        }

        private void AddItem(KeyValuePair<string, SkinInfo> s, EventHandler onclick)
        {
            SkinItem i = new SkinItem();
            i.Margin = new System.Windows.Forms.Padding(17, 15, 17, 15);
            i.Image = string.Format(s.Value.SkinFolderPath + "\\" + s.Value.Preview);
            i.ToolTip = s.Value.Name;
            i.SkinName = s.Key;
            i.SkinOptions = s.Value;

            i.BackColor = Color.FromArgb(0xDE, 0xDE, 0xDE);
            i.HoverBackColor = Color.FromArgb(0x0, 0x88, 0xff);
            i.ForeColor = Color.Black;
            i.HoverForeColor = Color.White;

            if (i.SkinName == MainForm._CurrentSkin.Name)
                i.IsCurrentSkin = true;

            if (onclick != null)
                i.Click += onclick;
            this.flowLayoutPanel1.Controls.Add(i);
        }
    }
}
