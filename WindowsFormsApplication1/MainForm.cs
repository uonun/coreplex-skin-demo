﻿/************************************************************************ 
 * CorePlex Skin Demo
 * Copyright (c) 2012 by uonun
 * Homepage: http://udnz.com
 * QQ: 25265411
 * Email:uonun@udnz.com
 ***********************************************************************
 * 推荐使用：CorePlex 在线代码库
 ***********************************************************************
 * 
 * CorePlex 是一款插件程序，是一款面向广大程序员，用于的 Micorosoft Visual Studio 插件，
 * 全面支持 Micorosoft Visual Studio 2005/2008/2010。它可以让您直接在 VS 中访问在线代码库。
 * 推荐功能： 
 *      一键将代码库中的代码插入到Visual Studio编辑器 
 *      将代码打包导出成 CHM 帮助文档（您甚至可以把它当做一个Chm打包器，将您的代码打包成Chm！）
 * 
 * CorePlex官方网站：http://www.udnz.com
 * 
 ***********************************************************************/

using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using CorePlex.Common;
using CorePlex.Controls;
using CorePlex.Core;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form, IBeautyForm
    {
        public MainForm()
        {

            InitializeComponent();

            // 加载设置
            _Settings = new CorePlex.Common.Settings();
            
            _bgForm = new BeautyForm(this, null);

            SkinInfo skininfo = new SkinInfo(_Settings);
            _CurrentSkin = skininfo.GetSkin(_Settings.SkinName);
            if (_CurrentSkin == null) _CurrentSkin = skininfo.GetFirst();

            // 应用皮肤
            _bgForm.SkinOptions = (SkinOptions)_CurrentSkin;

            this.ResumeLayout();
        }

        /// <summary>
        /// 应用皮肤
        /// </summary>
        private void ApplySkin()
        {
            try
            {
                if (!File.Exists(_CurrentSkin.BackgroundImagePath))
                {
                    MessageBox.Show("初始化皮肤失败，请检查皮肤配置或重新安装！", "CorePlex - 抱歉");
                }

                // 在淡入淡出的切换过程之间
                this._bgForm.OnSkinFadeout += (s, e1) =>
                {
                    // 应用前景窗体的皮肤设置
                    ApplyFrontSkin();
                };

                this.Opacity = (double)_CurrentSkin.Opacity_FrontForm / 255;

                _bgForm.ApplySkin((SkinOptions)_CurrentSkin);
            }
            catch (Exception ex)
            {
                MessageBox.Show("初始化皮肤失败，请检查皮肤配置或重新安装！", "CorePlex - 抱歉");
            }
        }

        /// <summary>
        /// 应用前景窗体的皮肤设置，要在切换皮肤时重新应用的图片，也必须在此处设置
        /// </summary>
        private void ApplyFrontSkin()
        {
            this.SuspendLayout();

            /*
             * 将MaximumSize值大的皮肤切换成MaximumSize值小的皮肤时，窗体内部控件的渲染会出问题。
             * 同时，未解决自适应皮肤大背景的问题，在大显示器分辨率的情况下，皮肤背景图可能不够布满屏幕，
             * 因此此处限制一个基本的MaximumSize。皮肤的背景图建议不小于此大小。
             */
            this.MaximumSize = this._bgForm.MaximumSize = new Size(1680, 1050);
            //this.MaximumSize = this._bgForm.MaximumSize = _CurrentSkin.MaximumSize;

            this.ResumeLayout();
        }

        #region IBeautyForm 成员

        void IBeautyForm.ApplySkin(SkinOptions newSkin)
        {
            MainForm._CurrentSkin = (SkinInfo)newSkin;
            this._bgForm.ApplySkin(newSkin);
        }

        SkinOptions IBeautyForm.SkinOptions
        {
            get
            {
                return this._bgForm.SkinOptions;
            }
        }

        #endregion

        /// <summary>
        /// 重写此属性
        /// </summary>
        public new FormWindowState WindowState
        {
            get { return this._bgForm.WindowState; }
            set
            {
                this._bgForm.WindowState = value;
            }
        }

        public new void Show()
        {
            if (_bgForm.WindowState == FormWindowState.Minimized)
                _bgForm.Show();
            base.Show();
        }


        /// <summary>
        /// 背景窗体
        /// </summary>
        private BeautyForm _bgForm;
        /// <summary>
        /// 当前皮肤
        /// </summary>
        public static SkinInfo _CurrentSkin = null;
        /// <summary>
        /// 当前配置
        /// </summary>
        public static Settings _Settings = null;
        /// <summary>
        /// 已打开的皮肤中心窗口
        /// </summary>
        public static SkinForm _OpenedSkinForm = null;

        private void button2_Click(object sender, EventArgs e)
        {
            if (_OpenedSkinForm == null)
            {
                _OpenedSkinForm = new SkinForm();
                _OpenedSkinForm.TopMost = true;
                _OpenedSkinForm.FormClosed += (s1, e1) =>
                {
                    _OpenedSkinForm = null;
                };
            }
            // 只能用 Show，不能用 ShowDialog
            _OpenedSkinForm.Show();
            _OpenedSkinForm.Activate();
        }

        private void isMarsk_CheckedChanged(object sender, EventArgs e)
        {
            this._bgForm.IsMarskEnabled = isMarsk.Checked;
            this._bgForm.Refresh();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            _CurrentSkin.Opacity_BgForm = _CurrentSkin.Opacity_FrontForm = (byte)trackBar1.Value;
            this._bgForm.Refresh();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.udnz.com/?skindemo");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("https://me.alipay.com/udnz");
        }
    }
}
